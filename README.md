# CiNAC Hardware (Anti-collision and Navigation Belt for Visually Impaired People)
Arduino code developed for the CiNAC project, made in C, using Arduino libraries. The project uses the following electronic devices:
* 4 Ultrassonic Sensors HC-SR04;
* 1 Sharp Infrared Sensor 0A41SKF-61;
* 1 Arduino Mega 2560;
* 5 Akiyama Vibra-call Motors;
* Jumpers, Protoboard and an Arduino USB Sensor (Basic electronics material);
The algorithm must take input from the sensors located in the belt and define a vibration percentage for each Vibra-call motor, exposing an obstacle to a blindman.