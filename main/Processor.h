//
// Created by luiscm on 04/06/17.
//

#ifndef ARDUINO_PROCESSADOR_H
#define ARDUINO_PROCESSADOR_H

void setup_kalman(int num_sensors, double constA,double constB, double constH, double constQ, double constR, double constPrevEst, double constPrevErr);

/*
 *  Fill vibrations array with new vibrations according to new distances
 *      of the inDistances array
 */
void calculate_vibrations(float* inDistances, float* vibrations);

/*
 *  Calculate vibrations with normalized value from 0 to 100 according with inDistance value( in mm )
 */
float _get_vibration(float inDistance);

float _filter(int index, float distance);

#endif //ARDUINO_PROCESSADOR_H

