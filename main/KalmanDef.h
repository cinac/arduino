//
// Created by Carlos Matheus Barros da Silva on 24/05/17.
//

#ifndef CINAC_SIMULATOR_KALMANVALUES_H
#define CINAC_SIMULATOR_KALMANVALUES_H


class KalmanDef {
private:
    double A,B,H,Q,R,Xk,Xk1,Pk,Pk1,Uk,Kk,Zk;
public:
    KalmanDef();
    ~KalmanDef();
    double getA();
    void setA(double a);
    double getB();
    void setB(double b);
    double getH();
    void setH(double h);
    double getQ();
    void setQ(double q);
    double getR();
    void setR(double r);
    double getPriorCurrentEstimation();
    void setPriorCurrentEstimation(double xk);
    double getPreviousEstimation();
    void setPreviousEstimation(double xk1);
    double getPriorCurrentError();
    void setPriorCurrentError(double pk);
    double getPreviousError();
    void setPreviousError(double pk1);
    double getCurrentControlSignal();
    void setCurrentControlSignal(double uk);
    double getCurrentKalmanGain();
    void setCurrentKalmanGain(double kk);
    double getCurrentMeasuredValue();
    void setCurrentMeasuredValue(double zk);
    double sum(double A, double B);
    double sub(double A, double B);
    double mult(double A, double B);
    double transp(double A);
    double inv(double A);
    double identity();
};


#endif //CINAC_SIMULATOR_KALMANVALUES_H
