//
// Created by luiscm on 04/06/17.
//
#include <Arduino.h>
#include <math.h>
#include <stdlib.h>
#include "Processor.h"
#include "KalmanFilter.h"

int numSensors;
KalmanFilter **filters;

static const float MAX = 300.0;

void setup_kalman(int num_sensors, double constA,double constB, double constH, double constQ, 
                  double constR, double constPrevEst, double constPrevErr)
{
   numSensors = num_sensors;
   filters = (KalmanFilter**) malloc(numSensors * sizeof(KalmanFilter*));
   
   int sensor;
   for(sensor=0; sensor<numSensors; sensor++)
      filters[sensor] = new KalmanFilter(constA,constB,constH,constQ,constR,constPrevEst,constPrevErr);
}

void calculate_vibrations(float* inDistances, float* vibrations)
{
    Serial.print("Distancias filtradas: ");
    for (int i = 1; i <= numSensors; ++i) {
        if (i == numSensors){
            /*
             * The middle sensor is an IR sensor. It will vibrate only
             * when the distance is less than 30mm because of the
             * specifications of the sensor used.
             */

            if (inDistances[i] <= 30)
                vibrations[i] = 100;
            else
                vibrations[i] = 0;
        }
        else{

            float newDistance = _filter(i, inDistances[i]>MAX?MAX:inDistances[i]);
            Serial.print(newDistance);
            Serial.print(" ");
             vibrations[i] = _get_vibration( newDistance );
        }
    }
   Serial.println("");
}

float _get_vibration(float inDistance)
{
    /*
     * The function v(x) that defines the vibration is:
     *
     *          v(x) := 100(1 - exp(Bt)),
     *
     *  where
     *
     *          t = 1/(MAX^D) - 1/(x^D),
     *
     *  and B, D are constants to be determined.
     */

    static const float NAPIER = 2.718281;

//    Function constants
    static const float B = 5e3;
    static const float D = 2.2;

    float exp;

    if (inDistance <= MAX)
        exp = B*(1.0/powf(MAX, D) - 1.0/powf(inDistance, D));
    else
        exp = 0;
    
    return 100.0*(1.0 - powf(NAPIER, exp));
}

float _filter(int index, float distance){
    return (float)filters[index]->filter(distance);
}


