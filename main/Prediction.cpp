//
// Created by Carlos Matheus Barros da Silva on 24/05/17.
//

#include "Prediction.h"
#include "KalmanDef.h"

Prediction::Prediction() {
}

Prediction::~Prediction() {

}

//    this method will make the follow operation:
//    xk = a*xk1 + b*uk
void Prediction::projectStateAhead (KalmanDef *values){
    xk = values->getPriorCurrentEstimation();
    a = values->getA();
    b = values->getB();
    uk = values->getCurrentControlSignal();
    xk1 = values->getPreviousEstimation();

    xk = values->sum( values->mult( a , xk1 ), values->mult( b , uk ) );

    values->setPriorCurrentEstimation( xk );
}

//    this method will make the follow operation:
//    pk = a*pk1*at + q
void Prediction::projectErrorAhead (KalmanDef *values){
    pk = values->getPriorCurrentError();
    a = values->getA();
    pk1 = values->getPreviousError();
    at = values->transp(a);
    q = values->getQ();

    pk = values->sum( values->mult( values->mult(a, pk1), at ), q );

    values->setPriorCurrentError( pk );
}