//
// Created by igor on 26/06/17.
//

#ifndef ARDUINO_KALMANDEF_H
#define ARDUINO_KALMANDEF_H

struct KalmanDef {
    double A,B,H,Q,R;
    double Xk,Xk1,Pk,Pk1,Uk,Kk,Zk;
}

double getA(KalmanDef* kalmanDef);
void setA(KalmanDef* kalmanDef, double a);
double getB(KalmanDef* kalmanDef);
void setB(KalmanDef* kalmanDef, double b);
double getH(KalmanDef* kalmanDef);
void setH(KalmanDef* kalmanDef, double h);
double getQ(KalmanDef* kalmanDef);
void setQ(KalmanDef* kalmanDef, double q);
double getR(KalmanDef* kalmanDef);
void setR(KalmanDef* kalmanDef, double r);



double getPriorCurrentEstimation(KalmanDef* kalmanDef);
void setPriorCurrentEstimation(KalmanDef* kalmanDef, double xk);
double getPreviousEstimation(KalmanDef* kalmanDef);
void setPreviousEstimation(KalmanDef* kalmanDef, double xk1);
double getPriorCurrentError(KalmanDef* kalmanDef);
void setPriorCurrentError(KalmanDef* kalmanDef, double pk);
double getPreviousError(KalmanDef* kalmanDef);
void setPreviousError(KalmanDef* kalmanDef, double pk1);
double getCurrentControlSignal(KalmanDef* kalmanDef);
void setCurrentControlSignal(KalmanDef* kalmanDef, double uk);
double getCurrentKalmanGain(KalmanDef* kalmanDef);
void setCurrentKalmanGain(KalmanDef* kalmanDef, double kk);
double getCurrentMeasuredValue(KalmanDef* kalmanDef);
void setCurrentMeasuredValue(KalmanDef* kalmanDef, double zk);
double sum(KalmanDef* kalmanDef, double A, double B);
double sub(KalmanDef* kalmanDef, double A, double B);
double mult(KalmanDef* kalmanDef, double A, double B);
double transp(KalmanDef* kalmanDef, double A);
double inv(KalmanDef* kalmanDef, double A);
double identity();

#endif //ARDUINO_KALMANDEF_H
