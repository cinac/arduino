//
// Created by felipe_coimbra on 01/07/2017
//

#ifndef MOTORS_CONTROLLER_H
#define MOTORS_CONTROLLER_H

/*
 *  Initializes motors and vibration parameters
 *      number:
 *          Number of motors to operate. Motors are initialized from the user's left to right.
 *      minimum:
 *          Minimum threshold vibration the motor can maintain without failing
 *      maximum:
 *          Maximum threshold vibration not to annoy user
 *      headstart:
 *          Initial peak of vibration necessary to overpass motor natural damping
 */
void setup_motors(int number, int minimum, int maximum, int headstart);

/*
 *  Updates motors' vibrations in vibrations array
 */
void update_motors(float *vibrations);

/*
 *  Update single motor with normalized value from minimum to maximum
 */
void _update(int motor, int value);

#endif
