#include <stdlib.h>
#include <Arduino.h>
#include "SensorReading.h"
#include "Processor.h"
#include "MotorsController.h"

#define INF 999999

/*
    Configurable options for sensor testing
*/
const int num_ultrasound = 4;
const int num_ir = 1;

/*
    Configurable options for motor testing
*/
const int num_motors = num_ir + num_ultrasound;
const int minVibration = 70;
const int maxVibration = 150;
const int headstartVibration = 200;

/*
    Configurable KalmanSensor constants for Kalman Filter testing
*/
const double constA = 1;
const double constB = 0;
const double constH = 1;
const double constQ = 0.1;
const double constR = 0.1;
const double constPrevEst = 100;
const double constPrevErr = 20;

/*
   Array with distances measured by sensors and
      vibrations calculated by processor
*/
float *distances;
float *vibrations;

void setup() {
  Serial.begin(9600);
  setup_sensors(num_ultrasound, num_ir);
  setup_kalman(num_ultrasound + num_ir, constA, constB, constH, constQ, constR, constPrevEst, constPrevErr);
  setup_motors(num_motors, minVibration, maxVibration, headstartVibration);

  distances = (float *)calloc(num_ultrasound + num_ir + 1, sizeof(float));
  vibrations = (float *)calloc(num_ultrasound + num_ir + 1, sizeof(float));
}

void loop() {
  // Fill distances array
  read_sensors(distances);


  int i;
  Serial.print("Distancias: ");
  for (i = 1; i <= num_motors; i++) {
    Serial.print(distances[i]);
    Serial.print(" ");
  }

 // Serial.println("   ");

  // Fill vibrations array
  calculate_vibrations(distances, vibrations);
  /*Serial.print("Vibracoes: ");
    for(i=1; i<=num_motors; i++){
    Serial.print(vibrations[i]);
    Serial.print(" ");
    }
    Serial.println("");*/

  // Update motors' vibration
  update_motors(vibrations);
 delay(20);
}
