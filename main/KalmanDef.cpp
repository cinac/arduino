//
// Created by Carlos Matheus Barros da Silva on 24/05/17.
//

#include "KalmanDef.h"

KalmanDef::KalmanDef(){
}
KalmanDef::~KalmanDef(){
}
double KalmanDef::getA(){
    return A;
}
void KalmanDef::setA(double a){
    A=a;
}
double KalmanDef::getB() {
    return B;
}
void KalmanDef::setB(double b){
    B=b;
}
double KalmanDef::getH(){
    return H;
}
void KalmanDef::setH(double h){
    H=h;
}
double KalmanDef::getQ(){
    return Q;
}
void KalmanDef::setQ(double q){
    Q=q;
}
double KalmanDef::getR(){
    return R;
}
void KalmanDef::setR(double r){
    R=r;
}
double KalmanDef::getPriorCurrentEstimation(){
    return Xk;
}
void KalmanDef::setPriorCurrentEstimation(double xk){
    Xk=xk;
}
double KalmanDef::getPreviousEstimation(){
    return Xk1;
}
void KalmanDef::setPreviousEstimation(double xk1){
    Xk1=xk1;
}
double KalmanDef::getPriorCurrentError(){
    return Pk;
}
void KalmanDef::setPriorCurrentError(double pk){
    Pk=pk;
}
double KalmanDef::getPreviousError(){
    return Pk1;
}
void KalmanDef::setPreviousError(double pk1){
    Pk1=pk1;
}
double KalmanDef::getCurrentControlSignal(){
    return Uk;
}
void KalmanDef::setCurrentControlSignal(double uk){
    Uk=uk;
}
double KalmanDef::getCurrentKalmanGain(){
    return Kk;
}
void KalmanDef::setCurrentKalmanGain(double kk){
    Kk=kk;
}
double KalmanDef::getCurrentMeasuredValue(){
    return Zk;
}
void KalmanDef::setCurrentMeasuredValue(double zk){
    Zk=zk;
}
double KalmanDef::sum(double A, double B){
    return A+B;
}
double KalmanDef::sub(double A, double B){
    return A-B;
}
double KalmanDef::mult(double A, double B){
    return A*B;
}
double KalmanDef::transp(double A){
    return A;
}
double KalmanDef::inv(double A){
    return (1/A);
}
double KalmanDef::identity(){
    return 1;
}