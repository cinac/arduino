//
// Created by Carlos Matheus Barros da Silva on 24/05/17.
//

#include "Filter.h"

Filter::Filter (){

}
Filter::~Filter(){

}

//    this method will make the follow operation:
//    kk = pk1*ht*(h*px*ht + r)^-1
void Filter::computeKalmanGain(KalmanDef *values){

    kk = values->getCurrentKalmanGain();
    pk = values->getPriorCurrentError();
    h = values->getH();
    ht = values->transp(h);
    r = values->getR();

    kk = values->mult( values->mult(pk,h), values->inv(values->sum( values->mult( values->mult( h, pk), ht ), r ) ) );

    values->setCurrentKalmanGain(kk);
}

//    this method will make the follow operation:
//    xkCurrent = xk + kk*(zk - h*xk);
double Filter::updateEstimation(KalmanDef *values){

    xk = values->getPriorCurrentEstimation();
    kk = values->getCurrentKalmanGain();
    zk = values->getCurrentMeasuredValue();
    h = values->getH();

    xkCurrent = values->sum( xk , values->mult(kk, values->sub(zk, values->mult( h , xk ) ) ) );

    //the actual estimation will be used in next filter step as the previous estimation
    values->setPreviousEstimation(xkCurrent);

    return xkCurrent;
}

//    this method will make the follow operation:
//    pkCurrent = (i - kk*h)*pk;
void Filter::upadteErrorCovariance(KalmanDef *values){

    i = values->identity();
    kk = values->getCurrentKalmanGain();
    h = values->getH();
    pk = values->getPriorCurrentError();

    pkCurrent = values->mult( values->sub ( i , values->mult( kk , h ) ), pk );

    //the actual error will be used in next filter step as the previous error
    values->setPreviousError(pkCurrent);
}
