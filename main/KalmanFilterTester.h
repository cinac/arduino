//
// Created by Carlos Matheus Barros da Silva on 17/06/17.
//

#ifndef CINAC_SIMULATOR_KALMANFILTERTESTER_H
#define CINAC_SIMULATOR_KALMANFILTERTESTER_H

#include "KalmanFilter.h"

class KalmanFilterTester {

public:
    KalmanFilterTester();
    ~KalmanFilterTester();
    void test();

private:
};


#endif //CINAC_SIMULATOR_KALMANFILTERTESTER_H
