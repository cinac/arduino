//
// Created by felipe_coimbra on 01/07/2017
//

#include "MotorsController.h"
#include <Arduino.h>

#define ANTIHORARIO true

int numMotors;
const int orientationPin[] = {7,8};
const int pwmPin[] = {0,2,3,4,5,6};
int minVibration;
int maxVibration;
int headstartVibration;

void setup_motors(int number, int minimum, int maximum, int headstart){
  if(number > 5){
    Serial.println("Numero de motores maior que o limite de 5");
    return;
  }
  
  numMotors = number;
  minVibration = minimum;
  maxVibration = maximum;
  headstartVibration = headstart;

  pinMode(orientationPin[0],OUTPUT);
  pinMode(orientationPin[1],OUTPUT);
  
  int motor;
  for (motor = 1;  motor <= numMotors; motor++)
    pinMode(pwmPin[motor], OUTPUT);

}

void update_motors(float *vibrations){
    digitalWrite(orientationPin[0],HIGH);
    digitalWrite(orientationPin[1],LOW);
    
    int motor;
    for(motor = 1; motor <= numMotors; motor++){
      if((int)vibrations[motor] > 0){
  
        _update(motor, map( (int)vibrations[motor],0,100,minVibration,maxVibration));
      }
      else
        analogWrite(pwmPin[motor],0);
    }
}

void _update(int motor, int value){
    analogWrite(pwmPin[motor],headstartVibration);
    delay(5);

  analogWrite(pwmPin[motor],value);
}




