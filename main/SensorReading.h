/*Arduino logic for CiNAC - Anti-Collision and Navigation Belt for Visually Impaired People
Conventions:

 (1) Each of the four ultrassonic sensors is numbered 1 - 5 (Most left to most right positioned on the belt). Each sensor uses two digital pins.
 For the sensor number i:
   - trigPin is the 2*i digital port;
   - echoPin is the 2*i + 1 digital port;

  (2) The Sharp Infrared sensor is the A0 port;
*/
#ifndef SENSOR_READING_H
#define SENSOR_READING_H

void _initialize(int i);

double _read_usensor(int cont);

double _read_irsensor();

void read_sensors(float *distance);

void setup_sensors(int num_ultra, int num_ir);

#endif
