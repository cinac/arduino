#include "SensorReading.h"
#include <Arduino.h>

#define INF 999999
  
int us_num;
int ir_num;
int ir_pin;

int *trigPin;
int *echoPin;
double * lastDistance;

/*
 *  Inicialize o i-esimo motor
 */
void _initialize(int i) {
   trigPin[i] = 2*i + 12;
   echoPin[i] = 2*i + 13;

   pinMode(trigPin[i], OUTPUT);
   pinMode(echoPin[i], INPUT);
}

double _read_usensor(int cont){
    register int i = 0;
    double distance;
    
    do{
      digitalWrite(trigPin[cont], LOW);
      delayMicroseconds(2);
    
      // Sets the trigPin on HIGH state for 10 micro seconds
      digitalWrite(trigPin[cont], HIGH);
      delayMicroseconds(10);
      digitalWrite(trigPin[cont], LOW);
    
      // Reads the echoPin, returns the sound wave travel time in microseconds
      double duration = pulseIn(echoPin[cont], HIGH, 1000000L)/(1000.0*10.0);
      
      // Calculating the distance

      distance = duration * 340.0 * 2.0;

      //Testing with NewPing Library
      //NewPing sonar(trigPin[cont], echoPin[cont], 200);
      //unsigned int t = sonar.ping();
      //distance = sonar.convert_cm(t);
     
    } while(i++ < 1 and distance >= 1e3);

    if (distance >= 1e3) {
      distance = lastDistance[cont];
    } else {
      lastDistance[cont] = distance;
    }
    
   /*if(cont == 1){ 
      Serial.print("Sensor ");
      Serial.print(cont);
      Serial.print(": ");
      Serial.println(distance);
   }*/

    return distance;
}

double _read_irsensor(){
 float volts = analogRead(ir_pin)*0.0048828125;
 double dist = 13*pow(volts, -1);

  if(dist <= 30) return dist;
  else return INF;
}

void read_sensors(float *distance){
    int cont;
    for(cont=1; cont<=us_num; cont++)
        distance[cont] = _read_usensor(cont);

    //distance[cont] = _read_irsensor();
}

void setup_sensors(int num_ultra, int num_ir){
    us_num = num_ultra;
    ir_num = num_ir;
    ir_pin = 0;

    trigPin = (int *) calloc(us_num+1, sizeof(int));
    echoPin = (int *) calloc(us_num+1, sizeof(int));
    lastDistance = (double *) calloc(us_num+1, sizeof(double));
    
    int i;
    //Initializes each sensor
    for(i=1; i<=us_num; i++)
        _initialize(i);
}
